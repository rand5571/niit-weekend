#include "VeryLongInt.h"
#include<iostream>

using namespace std;

int main() {
	char str1[] = "1234567";
	char str2[] = "19991";
	unsigned _int64 a1 = 1234567, a2 = 19991;
	VeryLongInt n1(str1), n2(str2);
	VeryLongInt n3= n1 + n2;
	VeryLongInt n4 = n1 - n2;
	VeryLongInt n5 = n1 * n2;
	cout << "Numbers" << endl;
	 n1.printNum();
	 n2.printNum();
	 cout << "Sum" << endl;
	n3.printNum();
	cout << "Check Sum" << endl;
	cout << a1+a2 << endl;
	cout << "Residual" << endl;
	n4.printNum();
	cout << "Check Residual" << endl;
	cout << a1 - a2 << endl;
	cout << "Multiplication" << endl;
	n5.printNum();
	cout << "Check Multiplication" << endl;
	cout << a1 * a2 << endl;
	n1 -= n2;
	n1.printNum();
	n1 += n2;
	n1.printNum();
	int k = (n1 < n2) ? 1 : 0;
	cout << k<<endl;
	k = (n1 == n3) ? 1 : 0;
	cout << k << endl;
	getchar();
	return 0;

}