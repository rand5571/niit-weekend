#include "VeryLongInt.h"
#include<iostream>

using namespace std;

 VeryLongInt operator+ (const VeryLongInt & n1, const VeryLongInt & n2)  {
	int new_size = (n1.getSize() >= n2.getSize()) ? (n1.getSize()+1) : (n2.getSize()+1);
	VeryLongInt n_temp(new_size - 1,0);
	VeryLongInt n_summ(new_size,0);


	if (n1>n2) {
		for (int i = 0; i < n2.getSize(); i++)
			n_temp.setNum(i, n2.getNum(i));
		int r = 0;
		for (int i = 0; i < new_size - 1; i++) {
			n_summ.setNum(i,n1.getNum(i) + n_temp.getNum(i)+r);
			if (n_summ.getNum(i) >= 10) {
				r = 1;
				n_summ.setNum(i, n_summ.getNum(i)-10);
			}
			else r = 0;
			}
	}
	else {
		for (int i = 0; i < n1.getSize(); i++)
			n_temp.setNum(i, n1.getNum(i));
		int r = 0;
		for (int i = 0; i < new_size - 1; i++) {
			n_summ.setNum(i, n2.getNum(i) + n_temp.getNum(i)+r);
			if (n_summ.getNum(i) >= 10) {
				r = 1;
				n_summ.setNum(i, n_summ.getNum(i) - 10);
			}
			else r = 0;
		}
	}
	
	return n_summ;
	}
 VeryLongInt operator- (const VeryLongInt & n1, const VeryLongInt & n2) {
	 VeryLongInt n_diff(n1.getSize(), 0),n_temp(n1.getSize(),0);
	 if (n1>n2) {
		 for (int i = 0; i < n2.getSize(); i++) {
			 n_temp.setNum(i, n2.getNum(i));
		 }
		 for (int i = 0; i < n1.getSize(); i++) {
			 
			 n_diff.setNum(i, n1.getNum(i));
		 }
		 
		 for (int i = 0; i < n1.getSize(); i++) {
			 n_diff.setNum(i, n_diff.getNum(i) - n_temp.getNum(i));
			 if (n_diff.getNum(i) < 0) {
				 n_diff.setNum(i, n_diff.getNum(i) + 10);
				 n_diff.setNum(i+1, n_diff.getNum(i+1) - 1);
			 }
		 }
	  }
	  return n_diff;
 }

 VeryLongInt operator* (const VeryLongInt & n1, const VeryLongInt & n2){
	 VeryLongInt n_mult(n1.getSize()+n2.getSize()+1, 0);

	 for (int i = 0; i < n2.getSize(); i++) 
	 for (int j = 0; j < n1.getSize(); j++) 
			 n_mult.setNum(i+j, n_mult.getNum(i+j) +n1.getNum(j)*n2.getNum(i));

			 for (int i = 0; i < n1.getSize() + n2.getSize(); i++) {
				 n_mult.setNum(i + 1, n_mult.getNum(i + 1) + n_mult.getNum(i) / 10);
				 n_mult.setNum(i, n_mult.getNum(i) % 10);
			 }
			 
		
	 
 return n_mult;
 }
 VeryLongInt& VeryLongInt:: operator=(const VeryLongInt & n1) {
	 if (*this != n1) {
		 size = n1.getSize();
		 delete[] arr;
		 for (int i = 0; i < size; i++)
			 setNum(i, n1.getNum(i));
	 }
		 return *this;
	 
}

 void VeryLongInt:: operator+=(const VeryLongInt & n) {
	 VeryLongInt temp = *this + n;
	 *this = temp;
	
 }
	
	 

 void VeryLongInt:: operator-=(const VeryLongInt & n) {

	 VeryLongInt temp = *this - n;
	 	 *this=temp;

 }
 void VeryLongInt::printNum() const
 {
	 int k = 0;
	 while (!arr[size - 1 - k]) k++;
	 for(int i=size-1-k ;i>=0;i--)
		 cout << arr[i];
	 cout << endl;
 }
  

 int VeryLongInt::getSize() const
 {
	 return size;
 }

 int VeryLongInt::getNum(int i) const
 {
	 int* temp = arr;
	 return *(temp+i);
 }
 void VeryLongInt::setNum(int i, int x)
 {
	 *(arr + i) = x;
 }

 bool operator< (const VeryLongInt & n1, const VeryLongInt & n2) {
	 
	 if (n1.getSize() > n2.getSize())
		 return false;
	 else  if (n1.getSize() < n2.getSize())
		 return true;
	 else {
		 for (int i = n1.getSize() - 1; i >= 0; i--) {
			 if (n1.getNum(i) > n2.getNum(i)) 
				 return false;
			 else
				 if (n1.getNum(i) < n2.getNum(i))
					 return true;
				 else continue;
				 }

		 return false;
	 }
 }

 bool operator> (const VeryLongInt & n1, const VeryLongInt & n2) {

	 if (n1.getSize() < n2.getSize())
		 return false;
	 else  if (n1.getSize() > n2.getSize())
		 return true;
	 else {
		 for (int i = n1.getSize() - 1; i >= 0; i--) {
			 if (n1.getNum(i) < n2.getNum(i))
				 return false;
			 else
				 if (n1.getNum(i) > n2.getNum(i))
					 return true;
				 else continue;
		 }

		 return false;
	 }
 }

 bool operator== (const VeryLongInt & n1, const VeryLongInt & n2) {

	 return !((n1<n2)&&(n1>n2));

}
 bool operator!= (const VeryLongInt & n1, const VeryLongInt & n2) {
	 	 
		 return !(n1==n2);
 }

 bool operator>= (const VeryLongInt & n1, const VeryLongInt & n2) {
	
		 return !(n1<n2);
 }

 bool operator<= (const VeryLongInt & n1, const VeryLongInt & n2) {

	 return !(n1>n2);
 }
