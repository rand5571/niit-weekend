#ifndef _DATETIME_H_
#define _DATETIME_H_
#define _CRT_SECURE_NO_WARNINGS
#include<ctime>



class DateTime
{
private:
	time_t sec;
	// static string WeekDays[7];
	// static string Months[12];

public:
	DateTime(int day, int month, int year) {
		struct tm * timeinfo;
		time(&sec);
		timeinfo = localtime(&sec);             
		timeinfo->tm_year = year - 1900;             
		timeinfo->tm_mon = month - 1;                 
		timeinfo->tm_mday = day;                 
		sec=mktime(timeinfo);
	}
	DateTime() :sec(time(0)) {}
	DateTime(const DateTime& ref);
	
	void getToday() const;
	void getYesterday() const;
	void getTommorow() const;
	void getFuture(int N_d) const;
	void getPast(int N_d) const;
	void getMonth() const;
	void getWeekDay() const;
	time_t getsec() const;
	void calcDifference(DateTime& ref);
};

#endif