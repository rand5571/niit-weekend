/*The first part of the program calculates difference in Earth's radius.
The second part of the program calculates total cost for concrete path and
fencing around pool with radius of 3 m .
The function get returns the value of radius if argument is 1,
the value of ference if argument is 2, and the value of area if argument is 3.
*/

#include "Circle.h"
#include <iostream> 
using namespace std;


int main()
{
	
	double R=6378100.,S,P_a=1000.,P_f=2000.;
	Circle value;
	value.reset();
	value.setRadius(R);
	value.setFerence(value.get(2)+1.);
	cout << "Difference in radius is "<< value.get(1) - R << endl;

	value.reset();
	R = 3.;
	value.setRadius(R);
	S = value.get(3);
	value.setRadius(R+1.);
	cout << "Total cost of concrete path is " << P_a*(value.get(3) - S) << endl;
	cout << "Total cost of fencing is " << P_f*(value.get(2)) << endl;
	
	return 0;

}